// CSS Color Names
// Compiled by @bobspace.
//
// A javascript array containing all of the color names listed in the CSS Spec.
// The full list can be found here: http://www.w3schools.com/cssref/css_colornames.asp
// Use it as you please, 'cuz you can't, like, own a color, man.

var CSS_COLOR_NAMES = ["White", "AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","#d6cbd3","#eca1a6","#bdcebe","#ada397","#d5e1df","#e3eaa7","#b5e7a0","#e6e2d3","#c4b7a6","#92a8d1","#deeaee","#b1cbbb","#eea29a","#d5f4e6","#80ced6","#fefbd8","#ffef96","#f4e1d2"];
