const TIMESTAMP = 'Timestamp';
const EMAIL = 'Email address';
const SECTOR = 'Settore';
const SUB_SECTOR = 'Sottosettore';
const TITLE = 'Titolo';
const ABSTRACT = 'Abstract breve';
const PDF = 'Materiale aggiuntivo';
const IMAGE = 'Immagine';
const OTHER = 'Altri relatori';
const TAG = 'Tag';

const GDRIVELINK = 'https://docs.google.com/spreadsheets/d/1SkwjIoNgrq8Z7ezirGZqux0HWI-n7vS1lTkLNrSAceQ/edit?usp=sharing';