/*
    Showcase library: copyright (C) 2016 by giovanni.organtini@uniroma1.it

    This file is part of Showcase.
    
    Showcase is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Showcase is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with Showcase.  If not, see <http://www.gnu.org/licenses/>.
*/

function formattedCurrentDate() {
    /* return the current date */
    var currentDate = aYearBefore();
    var y = currentDate.replace(/-.*/, '');
    y = parseInt(y) + 1;
    currentDate = currentDate.replace(/^[0-9]{4}/, y);
    return currentDate;
}

function aYearBefore() {
    /* return the current date one year before now in the format YYYY-MM-DD */
    var currentdate = new Date();
    var yy = currentdate.getFullYear() - 1;
    var mm = currentdate.getMonth() + 1;
    if (mm < 10) {
	mm = '0' + mm;
    }
    var dd = currentdate.getDate();
    if (dd < 10) {
	dd = '0' + dd;
    }
    var mindate = yy + '-' + mm + '-' + dd;
    return mindate;
}

function mkArticle(i, data, h, index) {
    /* build the actual article window based on row i of table data. h is a hash with the
       colors */
    var evenOdd = i % 2;
    var leftRight = 'left';
    if (evenOdd == 1) {
	leftRight = 'right';
    }
    var article = '<article><p class=\'title\'>' + data.getValue(i, index[TITLE]) + '</p>';
    var img = data.getValue(i, index[IMAGE]);
    if (img != null) {
	img = img.replace('open?', 'uc?export=view&');
	article += '<div class=\'image-wrapper\'><img src=\'' +img + '\' /></div>';
    }
    article += '<p class=\'content\' style=\'background-color:' + h[data.getValue(i, index[SUB_SECTOR])] + 
    	';\'>' + data.getValue(i, index[ABSTRACT]);
    var more = data.getValue(i, index[PDF]);
    if (more != null) {
    	article += '<br><br><a href=\'' + more + '\' target=\'moreWindow\'>read more...</a>';
    }
    article += '</p>';
    var email = data.getValue(i, index[EMAIL]);
    var moreemail = data.getValue(i, index[OTHER]);
    if (moreemail != null) {
    	email += ", " + moreemail;
    }
    article += '<p class=\'link\' style=\'background-color:' + h[data.getValue(i, index[SUB_SECTOR])] + 
	';\'>for more info: <a href=\'mailto:' + email + '\' target=\'mailWindow\'>' + 
	email + '</a><br>';
    var tag = data.getValue(i, index[TAG]);
    if (tag != null) {
    	article += "<b>Tag: </b>" + tag;
    }
    var expire = "";
//  expire = data.getValue(i, index[TIMESTAMP]);
    article += '&nbsp;<span align="right">' + expire + '</span></p></article>';
    $('#' + leftRight).append(article);
}

function randomize(data) {
    /* shuffle the rows of a data table */
    var rc = data.addColumn('number', 'random');
    var n = data.getNumberOfRows();
    for (i = 0; i < n; i++) {
	data.setValue(i, rc, n*Math.random());
    }
    data.sort(rc);
    return data;
}

function colorHash(data, index) {
    /* return a hash associating keys to colors */
    var subsector = [];
    var n = data.getNumberOfRows();
    for (i = 0; i < n; i++) {
	var r = data.getValue(i, index[SUB_SECTOR]);
        if (r == null) {
	    r = 'none';
	}
	if (r.indexOf(',') >= 0) {
            var rs = r.split(',');
            for (j = 0; j < rs.length; j++) {
		subsector.push(rs[j].trim());
            }
	} else {
            subsector.push(r.trim());
	}
    }
    subsector.sort();
    var h = {};
    var color = 0;
    for (i = 0; i < subsector.length; i++) {
	if (h[subsector[i]] == null) {
	    h[subsector[i]] = CSS_COLOR_NAMES[color++];
	}
    }
    return h;
}

function buildLegend(h) {
    /* build the legend panel */
    var legend = '<TABLE><CAPTION>Legenda</CAPTION><TR>';
    for (var k in h) {
	if (h.hasOwnProperty(k)) {
	    legend += '<TD BGCOLOR=\'' + h[k] + '\'>' + k + '</TD>';
	}
    }
    legend += '</TR></TABLE></P>';
    $('#emptyarticle').html(legend);
}
